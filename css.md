title=css颜色模型
desc=color表示一种标准RGB色彩空间,一个颜色可以包括一个alpha通道，来表明颜色如何与它的背景色混合,
tag=css,颜色

===== 正文内容分割符号 ======

CSS 数据类型**color**表示一种标准RGB色彩空间,一个颜色可以包括一个*alpha*通道，来表明颜色如何与它的背景色混合

表示方式
1. 使用一个关键字
2. 使用RGB立体坐标系统（以“#”加十六进制或者rgb() 和rgba()函数表达式的形式）
3. 使用HSL圆柱坐标系统（以hsl()和hsla()函数表达式的形式）

### 颜色关键字
是不区分大小写的标识符，它表示一个具体的颜色

**transparent** 关键字表示一个完全透明的颜色，即该颜色看上去将是背景色。从技术上说，它是带有阿尔法通道为最小值的黑色，是 rgba(0,0,0,0) 的简写

<details>
<summary>
颜色关键色枚举 css1 <b>16种</b>css2 <b>1</b>种
</summary>

|颜色关键字| 中文名| 颜色值 | css版本 | 
| - | - | - | - |
| black | 黑色 | rgb( 0,   0,   0) | css1 | 
| silver | 银白色 |rgb( 192, 192, 129) | css1 | 
| gray | 灰色 |rgb( 128, 128, 128) | css1 | 
| white | 白色 |rgb( 255, 255, 255) | css1 | 
| maroon | 褐红色 |rgb( 128,   0,   0) | css1 | 
| red | 红色 |rgb( 255,   0,   0) | css1 | 
| purple | 紫色 |rgb( 128,   0, 128) | css1 | 
| fuchsia | 紫红色 |rgb(255,   0, 255 ) | css1 | 
| green | 绿色 |rgb( 0, 128,   0) | css1 | 
| lime | 石灰色 |rgb( 0, 255,   0 ) | css1 | 
| olive | 橄榄色 |rgb( 128, 128,   0) | css1 | 
| yellow | 黄色 |rgb( 255, 255,   0 ) | css1 | 
| navy | 深蓝色 |rgb( 0,   0, 128) | css1 | 
| blue | 蓝色 |rgb( 0,   0, 255) | css1 | 
| teal | 蓝绿色 |rgb( 0, 128, 128) | css1 | 
| aqua | 浅绿色 | rgb( 0, 255, 255) | css1 | 
| orange | 橙色 | rgb( 255, 165,   0) | css2| 
|aliceblue      |爱丽丝蓝      |rgb(240, 248, 245)| css3 |
|antiquewhite   | 古董白     |rgb(250, 235, 215)| css3 |
|aquamarine     | 碧绿色     |rgb(127, 255, 212)| css3 |
|azure          |天蓝色      |rgb(240, 255, 255)| css3 |
|beige          | 淡棕色     |rgb(245, 245, 220)| css3 |
|bisque         | 橘黄色     |rgb(255, 228, 196)| css3 |
|blanchedalmond | 白杏色     |rgb(255, 235, 205)| css3 |
|blueviolet     | 蓝紫色     |rgb(138,  43, 226)| css3 |
|brown          | 褐色    |rgb(165,  42,  42)| css3 |
|burlywood      | 实木色     |rgb(222, 184,  35)| css3 |
|cadetblue      | 军蓝色     |rgb( 95, 158, 160)| css3 |
|chartreuse     | 黄绿色     |rgb(127, 255,   0)| css3 |
|chocolate      | 深褐色     |rgb(210, 105,  30)| css3 |
|coral          | 橘红色     |rgb(255, 127,  80)| css3 |
|cornflowerblue | 浅蓝色     |rgb(100, 149, 237)| css3 |
|cornsilk       | 玉米色     |rgb(255, 248, 220)| css3 |
|crimson        | 深红色    |rgb(220,  20,  60)| css3 |
|darkblue       | 深蓝色     |rgb(  0,   0, 139)| css3 |
|darkcyan       | 深青色     |rgb(  0, 139, 139)| css3 |
|darkgoldenrod  | 暗金黄     |rgb(184, 134,  11)| css3 |
|darkgray       | 深灰色     |rgb(169, 169, 169)| css3 |
|darkgreen      | 深绿色    |rgb(  0, 100,   0)| css3 |
|darkkhaki      | 暗卡其色     |rgb(189, 183, 107)| css3 |
|darkmagenta    | 暗洋红色     |rgb(139,   0, 139)| css3 |
|darkolivegreen | 暗橄榄绿色     |rgb( 85, 107,  47)| css3 |
|darkorange     | 暗橙色     |rgb(255, 140,   0)| css3 |
|darkorchid     | 暗紫色    |rgb(153,  50, 204)| css3 |
|darkred        | 暗红色     |rgb(139,   0,   0)| css3 |
|darksalmon     | 暗橙红色     |rgb(233, 150, 122)| css3 |
|darkseagreen   | 深绿色    |rgb(143, 188, 143)| css3 |
|darkslateblue  | 暗灰蓝色     |rgb( 72,  61, 139)| css3 |
|darkslategray  | 墨绿色      |rgb( 47,  79,  79)| css3 |
|darkturquoise  |  --  |rgb(  0, 206, 209)| css3 |
|darkviolet     |  --  |rgb(148,   0, 211)| css3 |
|deeppink       |  --  |rgb(255,  20, 147)| css3 |
|deepskyblue    |  --  |rgb(  0, 191, 255)| css3 |
|dimgray        |  --  |rgb(105, 105, 105)| css3 |
|dimgrey        |  --  |rgb(105, 105, 105)| css3 |
|dodgerblue     |  --  |rgb( 30, 144, 255)| css3 |
|firebrick      |  --  |rgb(178,  34,  34)| css3 |
|floralwhite    |  --  |rgb(255, 250, 240)| css3 |
|forestgreen    |  --  |rgb( 34, 139,  34)| css3 |
|gainsboro      |  --  |rgb(220, 220, 220)| css3 |
|ghostwhite     |  --  |rgb(248, 248, 255)| css3 |
|gold           |  --  |rgb(255, 215,   0)| css3 |
|goldenrod      |  --  |rgb(218, 165,  32)| css3 |
|greenyellow    |  --  |rgb(173, 255,  47)| css3 |
|grey           |  --  |rgb(128, 128, 128)| css3 |
|honeydew       |  --  |rgb(240, 255, 240)| css3 |
|hotpink        |  --  |rgb(255, 105, 180)| css3 |
|indianred      |  --  |rgb(205,  92,  92)| css3 |
|indigo         |  --  |rgb( 75,   0, 130)| css3 |
|ivory          |  --  |rgb(255, 255, 240)| css3 |
|khaki          |  --  |rgb(240, 230, 140)| css3 |
|lavender       |  --  |rgb(230, 230, 250)| css3 |
|lavenderblush  |  --  |rgb(255, 240, 245)| css3 |
|lawngreen      |  --  |rgb(124, 252,   0)| css3 |
|lemonchiffon   |  --  |rgb(255, 250, 205)| css3 |
|lightblue      |  --  |rgb(173, 216, 230)| css3 |
|lightcoral     |  --  |rgb(240, 128, 128)| css3 |
|lightcyan      |  --  |rgb(224, 255, 255)| css3 |
|lightgoldenrodyellow|  --     |rgb(250, 250, 210)| css3 |
|lightgray      |  --     |rgb(211, 211, 211)| css3 |
|lightgreen     |  --     |rgb(144, 238, 144)| css3 |
|lightgrey      |  --     |rgb(211, 211, 211)| css3 |
|lightpink      |  --     |rgb(255, 182, 193)| css3 |
|lightsalmon    |  --     |rgb(255, 160, 122)| css3 |
|lightseagreen  |   --    |rgb( 32, 178, 170)| css3 |
|lightskyblue   |   --    |rgb(135, 206, 250)| css3 |
|lightslategray |  --     |rgb(119, 136, 153)| css3 |
|lightslategrey |  --     |rgb(119, 136, 153)| css3 |
|lightsteelblue |  --     |rgb(176, 196, 222)| css3 |
|lightyellow    |  --     |rgb(255, 255, 224)| css3 |
|limegreen      |  --     |rgb( 50, 205,  50)| css3 |
|linen          | --      |rgb(250, 240, 230)| css3 |
|mediumaquamarine | --      |rgb(102, 205, 170)| css3 |
|mediumblue     |  --     |rgb(  0,   0, 205)| css3 |
|mediumorchid   | --      |rgb(186,  85, 211)| css3 |
|mediumpurple   |  --     |rgb(147, 112, 219)| css3 |
|mediumseagreen |  --     |rgb( 60, 179, 113)| css3 |
|mediumslateblue|  --     |rgb(123, 104, 238)| css3 |
|mediumspringgreen| --      |rgb(  0, 250, 154)| css3 |
|mediumturquoise|  --     |rgb( 72, 209, 204)| css3 |
|mediumvioletred|  --     |rgb(199,  21, 133)| css3 |
|midnightblue   |  --     |rgb( 25,  25, 112)| css3 |
|mintcream      |  --     |rgb(245, 255, 250)| css3 |
|mistyrose      |  --     |rgb(255, 228, 225)| css3 |
|moccasin       |  --     |rgb(255, 228, 181)| css3 |
|navajowhite    |  --     |rgb(255, 222, 173)| css3 |
|oldlace        |  --     |rgb(253, 245, 230)| css3 |
|olivedrab      |  --     |rgb(107, 142,  35)| css3 |
|orangered      |  --     |rgb(255,  69,   0)| css3 |
|orchid         |  --     |rgb(218, 112, 214)| css3 |
|palegoldenrod  |  --     |rgb(238, 232, 170)| css3 |
|palegreen      |  --     |rgb(152, 251, 152)| css3 |
|paleturquoise  | --      |rgb(175, 238, 238)| css3 |
|palevioletred  |  --     |rgb(219, 112, 147)| css3 |
|papayawhip     |  --     |rgb(255, 239, 213)| css3 |
|peachpuff      |  --     |rgb(255, 218, 185)| css3 |
|peru           |  --     |rgb(205, 133,  63)| css3 |
|pink           |  --     |rgb(255, 192, 203)| css3 |
|plum           |  --     |rgb(221, 160, 221)| css3 |
|powderblue     |  --     |rgb(176, 224, 230)| css3 |
|rosybrown      |  --     |rgb(188, 143, 143)| css3 |
|royalblue      |  --     |rgb( 65, 105, 225)| css3 |
|saddlebrown    |  --     |rgb(139,  69,  19)| css3 |
|salmon         |  --     |rgb(250, 128, 114)| css3 |
|sandybrown     |  --     |rgb(244, 164,  96)| css3 |
|seagreen       |  --     |rgb( 46, 139,  87)| css3 |
|seashell       |  --     |rgb(255, 245, 238)| css3 |
|sienna         |  --     |rgb(160,  82,  45)| css3 |
|skyblue        | --      |rgb(135, 206, 235)| css3 |
|slateblue      | --      |rgb(106,  90, 205)| css3 |
|slategray      | --      |rgb(112, 128, 144)| css3 |
|slategrey      | --      |rgb(112, 128, 144)| css3 |
|snow           | --      |rgb(255, 250, 250)| css3 |
|springgreen    |  --     |rgb(  0, 255, 127)| css3 |
|steelblue      |  --     |rgb( 70, 130, 180)| css3 |
|tan            | --      |rgb(210, 180, 140)| css3 |
|thistle        |  --     |rgb(216, 191, 216)| css3 |
|tomato         | --      |rgb(255,  99,  71)| css3 |
|turquoise      | --     |rgb( 64, 224, 208)| css3 |
|violet         | 紫罗兰色     |rgb(238, 130, 238)| css3 |
|wheat          | 小麦色     |rgb(245, 222, 179)| css3 |
|whitesmoke     | 烟白色     |rgb(245, 245, 245)| css3 |
|yellowgreen    | 黄绿色      |rgb(154, 205,  50)| css3 |
</details>

### RGB立体坐标系统

可以使用红 - 绿 - 蓝（red-green-blue (RGB)）模式定义

1. 十六进制符号：#RRGGBB[AA]
2. 十六进制符号：#RGB[A]
3. 函数符：rgb[a](R, G, B[, A])
4. 函数符：rgb[a](R G B[ / A])

### HSL圆柱坐标系统
可以使用色相-饱和度-亮度（Hue-saturation-lightness）模式定义

1. 函数符：hsl[a](H, S, L[, A])
2. 函数符：hsl[a](H S L[ / A])

### RGB颜色转换HSL

1. 颜色值归一化：将RGB值转化为0~1之间的数值
2. 求色相H：确定RGB各通道的大小值
> 如果最大值为R H = (G-B)/(max-min)
> 如果最大值为G H = 2.0 + (B - R)/(max-min)
> 如果最大值为B H = 4.0 + (R -G)/(max-min)
> 如果最大值等于最小值则色相为0
> 色相值结果除6加1确保色相介于0到1之间

3. 求饱和度S 
> 如果最大值等于最小值则S = 0
> S = (max-min)/ max

4. 计算亮度L 
> L =(max + min) / 2